﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DLL2lib
{
    public static class ExportHelper
    {
        // https://stackoverflow.com/questions/18249566/c-sharp-get-the-list-of-unmanaged-c-dll-exports

        [DllImport("dbghelp.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SymInitialize(IntPtr hProcess, string UserSearchPath, [MarshalAs(UnmanagedType.Bool)]bool fInvadeProcess);

        [DllImport("dbghelp.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SymCleanup(IntPtr hProcess);

        [DllImport("dbghelp.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern ulong SymLoadModuleEx(IntPtr hProcess, IntPtr hFile,
            string ImageName, string ModuleName, long BaseOfDll, int DllSize, IntPtr Data, int Flags);

        [DllImport("dbghelp.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SymEnumerateSymbols64(IntPtr hProcess,
            ulong BaseOfDll, SymEnumerateSymbolsProc64 EnumSymbolsCallback, IntPtr UserContext);

        private delegate bool SymEnumerateSymbolsProc64(string SymbolName,
            ulong SymbolAddress, uint SymbolSize, IntPtr UserContext);

        private static List<string> _exports;

        private static bool EnumSyms(string name, ulong address, uint size, IntPtr context)
        {
            _exports.Add(name);
            return true;
        }

        public static List<string> GetExportedSymbols(string dllFile)
        {
            _exports = new List<string>();
            
            IntPtr hCurrentProcess = Process.GetCurrentProcess().Handle;

            // Initialize sym.
            // Please read the remarks on MSDN for the hProcess
            // parameter.
            bool status = SymInitialize(hCurrentProcess, null, false);

            if (status == false)
            {
                Debug.WriteLine("Failed to initialize sym.");
                return null;
            }

            // Load dll.
            ulong baseOfDll = SymLoadModuleEx(hCurrentProcess,
                IntPtr.Zero,
                dllFile,
                null,
                0,
                0,
                IntPtr.Zero,
                0);

            if (baseOfDll == 0)
            {
                Debug.WriteLine("Failed to load module.");
                SymCleanup(hCurrentProcess);
                return null;
            }

            // Enumerate symbols. For every symbol the 
            // callback method EnumSyms is called.
            if (SymEnumerateSymbols64(hCurrentProcess,
                    baseOfDll, EnumSyms, IntPtr.Zero) == false)
            {
                Debug.WriteLine("Failed to enum symbols.");
            }

            // Cleanup.
            SymCleanup(hCurrentProcess);

            return _exports;
        }
    }
}
