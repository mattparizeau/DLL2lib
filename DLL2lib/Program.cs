﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL2lib
{
    public class Program
    {
        public static void Main(string[] args)
        {
            foreach (string arg in args)
            {
                string inputDLL = Path.GetFullPath(arg);

                Console.WriteLine("Loading " + Path.GetFileName(inputDLL));

                if (!File.Exists(inputDLL))
                {
                    Console.WriteLine("File does not exist! Skipping...");
                    continue;
                }

                List<string> exports = ExportHelper.GetExportedSymbols(inputDLL);

                string defFile = Path.ChangeExtension(inputDLL, "def");
                /*if (defFile == null)
                {
                    Console.WriteLine("Internal Error");
                    continue;
                }*/

                Stream s = File.Open(defFile, FileMode.Create, FileAccess.Write);
                StreamWriter sw = new StreamWriter(s);

                sw.WriteLine("LIBRARY " + Path.GetFileNameWithoutExtension(inputDLL));
                sw.WriteLine("EXPORTS");
                foreach (string export in exports)
                {
                    sw.WriteLine("\t" + export);// + "=" + Path.GetFileNameWithoutExtension(inputDLL) + "_original." + export);
                }
                sw.WriteLine();

                sw.Flush();
                sw.Close();
                s.Close();

                Console.WriteLine("Wrote " + Path.GetFileName(defFile));

                string libFile = Path.ChangeExtension(inputDLL, "lib");

                string err =
                    "Error: lib.exe was not found on PATH. Make sure to run this tool from a Visual Studio Developer Command Prompt!";

                try
                {
                    ProcessStartInfo info = new ProcessStartInfo("lib.exe",
                        "/def:\"" + defFile.Replace('/', '\\') + "\" /OUT:\"" + libFile + "\"");
                    info.RedirectStandardError = false;
                    info.RedirectStandardOutput = false;
                    info.UseShellExecute = false;
                    Process p = Process.Start(info);

                    if (p == null)
                    {
                        Console.WriteLine(err);
                        return;
                    }
                    p.WaitForExit();
                }
                catch (Win32Exception)
                {
                    Console.WriteLine(err);
                    return;
                }

                //Console.WriteLine("Generated " + Path.GetFileName(libFile));
            }
        }
    }
}
